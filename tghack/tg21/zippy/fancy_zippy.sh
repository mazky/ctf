#!/bin/bash


function zippy(){
  echo "Unpacking a lot of files..."
  for i in {1337..1};
    do unzip "archive$i.zip"# > /dev/null 2>&1
  done
  echo "Unpacking completed."
  cat flag.txt
  
  echo "Cleaning up the mess..."
  
  mv archive1337.zip bak_archive1337.zip
  rm archive* 2>/dev/null
  mv bak_archive1337.zip archive1337.zip
  
  echo "There we go, all done."

}

if [[ -f flag.txt ]]; then
  echo "flag.txt already present."
else
  zippy
fi


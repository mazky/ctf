#!/bin/bash

for j in {101..1};
  do 
    for i in $(cat passwordlist.txt)
      do unzip -o -P $i archive$j.zip
    done
done

if [[ -f flag.txt ]]; then
  echo "Flag found!"
  cat flag.txt
else
  echo "Hmm, something went wrong. Is the .zip file here?"
fi

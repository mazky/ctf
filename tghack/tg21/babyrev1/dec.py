#from secret import flag

flag = open('enc.txt', 'r')

dec = []
for i in flag:
    #enc.append(hex(ord(i)%1000+0x00))
   # dec.append(chr(int(i)))
    dec.append(chr(int(i)))
            # make hex # str to number

    # ord - character to integer
    # chr - hex to str? 0x54 -> 'T'

for e in dec:
    print(e)

#
#>>> chr(int('0x00'))
#Traceback (most recent call last):
#      File "<stdin>", line 1, in <module>
#      ValueError: invalid literal for int() with base 10: '0x00'
#
#>>> type(0x00)
#<class 'int'>
#>>> type('0x00')
#<class 'str'>


#
#>>> 123%1000+0x00
#123
#>>> 123%1000
#123

#!/usr/bin/env python3

from pwn import *
import base64

host = 'are-ye-pirate.tghack.no'
port = 1337


def pirate(host, port):

    r = remote(host, port)
    r.recv().decode("utf-8")
#    print(r.recv().decode("utf-8"))
    r.send("asd\n")
 #   print(r.send("a\n"))

    nr = 1
    print("before loop starts")
    while True: # while there's more puzzles
        print("loop initiated")
        if nr == 1:
            b64_string = r.recv().decode("utf-8").split('\n')[1].split()[2] # isolate base64 part - fails on round 2 - line 1 on 2nd.
            print("1st run: " + b64_string)
            nr += 1
        else:
            print("started with else")
            
            print(r.recv()) #.decode("utf-8"))
            print("recv done")
            b64_string = r.recv().decode("utf-8").split()[2] # third word in string
            print("2nd run1: " + r.recv().decode("utf-8"))
            print("2nd run2: " + b64_string) #EOL error?
            
            reply = base64.b64decode(b64_string).decode("utf-8")
            print("created reply")
            r.send(str(reply) + "\n")
            print("sent reply")
            print(r.recv().decode("utf-8"))
            nr += 1
        reply = base64.b64decode(b64_string).decode("utf-8")

        r.send(str(reply) + "\n") # send back the decoded string
        print(r.send(str(reply) + "\n")) # send back the decoded string

        print(r.recv().decode("utf-8"))
        
        nr += 1
        print("end of loop")

pirate(host, port)



ida64

call    init
mov     eax, 0
call    check1
mov     [rbp+var_124], eax
mov     eax, 0
call    check2
mov     [rbp+var_120], eax
mov     eax, 0
call    check3
mov     [rbp+var_11C], eax
lea     rax, aYouVePassedAll ; "You've passed all the checks! Please be"...
mov     rdi, rax        ; s
call    _puts
lea     rax, aLoadingFlagThi ; "Loading flag... (this may or may not ta"...
mov     rdi, rax        ; s
call    _puts
mov     eax, [rbp+var_124]
imul    eax, [rbp+var_120]
imul    eax, [rbp+var_11C]
mov     edi, eax        ; seconds
call    _sleep
lea     rax, modes      ; "r"
mov     rsi, rax        ; modes
lea     rax, filename   ; "flag.txt"


So we want to reach flag.txt at 0x2155, I believe.

.rodata:0000000000002155 ; const char filename[]
.rodata:0000000000002155 filename        db 'flag.txt',0         ; DATA XREF: main+97↑o



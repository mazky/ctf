"no strings" was the first (100pts) challenge in the reversing category.

Open nostrings binary in IDA
Go to Exports tab

See "flag" in the list.

    flag	0000000000004050

Double click on it.

Read the flag:

    .data:0000000000004050                 public flag
    .data:0000000000004050 flag            dq offset aDuctfStringent
    .data:0000000000004050                                         ; DATA XREF: main+57↑r
    .data:0000000000004050 _data           ends                    ; "DUCTF{stringent_strings_string}"
    .data:0000000000004050

I did try to access 0x4050 in non-IDA ways, but was only able to find the flag this way.


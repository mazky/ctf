#!/usr/bin/env python3

from pwn import *
import urllib.parse
import base64

host = 'pwn-2021.duc.tf'
port = 31905

r = remote(host, port)

print(r.recvuntil(b'Press enter'))
print(r.send('\n'))
print(r.recvuntil(b'1+1'))
print(r.send('2\n'))

print("base 10")
base_10_q = r.recvuntil(b'(base 10): ')
base_10 = r.recvuntil(b'\n').decode('utf-8')
base_10_answer = int(base_10, 16)
r.send(f"{base_10_answer}\n")

print("aschii decode")
print(r.recvuntil(b'original ASCII letter: '))
q_ascii = r.recvuntil(b'\n').decode('utf-8')
q_ascii_answer = bytes.fromhex(q_ascii).decode('utf-8')
r.send(f"{q_ascii_answer}\n")

print("find original ascii symbols from url")
print(r.recvuntil(b'original ASCII symbols: '))
url_ascii = r.recvuntil(b'\n').decode('utf-8')
url_ascii_answer = urllib.parse.unquote(url_ascii)
r.send(f"{url_ascii_answer}\n")

print("base64 to plain")
print(r.recvuntil(b'me the plaintext: '))
base64_plain = r.recvuntil(b'\n')
base64_plain_answer = base64.b64decode(base64_plain).decode('utf-8')
print(f"{base64_plain_answer}")
#r.send(f"{base64_plain_answer}\n")

# not working, but base64 seems okay..
r.interactive()

# carked comes before any prompt. no place to provide the input...?
# confused.


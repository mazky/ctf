Sub folders will contain notes, and attempts that may or may not have ended in success.

Attempting to get a bit more active on CTFs, I'll list the more recent ones here.
Writeups will either be under here, or under the specific CTF's folder.

DownUnderCTF - ductf2021

misc/
The Introduction (This one is effectively free, nc and wait.)

reversing/
no strings

forensics/
Retro!

cloud/
Bad Bucket
